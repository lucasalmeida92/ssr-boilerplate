import { createGlobalStyle } from 'styled-components';

const colors = {
  fontColor: '#2c3e50',
  bgColor: '#ecf0f1',
};

const GlobalStyles = createGlobalStyle`
  body {
    margin: 0;
    background-color: ${colors.bgColor};
    color: ${colors.fontColor};
    font-family: 'Lato', Arial;
  }
`;

export default GlobalStyles;
